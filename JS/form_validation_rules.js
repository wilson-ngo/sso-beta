/*

-- Form Validation Rules -- 

*/	
	
	
// alpha and special chars only
$.validator.addMethod('alphaSpecial', function(value, element, params) {
	var regex = /^[ A-Za-z_@./#&+-]*$/
	return (regex.test(value));
	
},'Please enter alpha or special characters only');


// Australian phone number
jQuery.validator.addMethod('phoneAU', function(value, element) {

	//Regex too restrictive - /^(\+?61|0)\d{9}$/ 
	//use this instead /^[\d\s()+-]+$/
	
	return this.optional(element) || /^[\d\s()+-]+$/.test(value.replace(/\s+/g, ""));
}, "Please specify a valid phone number");



//email - matrix regex pattern --  ^([\da-zA-Z-_+][\da-zA-Z-_+.\w\']*[\da-zA-Z-_+]@[\da-zA-Z][\'-.\w]*[\da-zA-Z]\.[a-zA-Z]{2,7})$
//original pattern -- /^(\+?61|0)\d{9}$/.test(value.replace(/\s+/g, ""));

//matrix email validation
jQuery.validator.addMethod('matrixEmail', function(value, element) {
	return this.optional(element) || /^([\da-zA-Z-_+][\da-zA-Z-_+.\w\']*[\da-zA-Z-_+]@[\da-zA-Z][\'-.\w]*[\da-zA-Z]\.[a-zA-Z]{2,7})$/.test(value.replace(/\s+/g, ""));
}, "Please enter a email address");



//calculates complexity score for website request and adds it to a hidden field
var calcScore = function(){
	
	var totalScore = 0;
	
	$('.complexity-factor select').each(function(e){
		
		var elmVal = parseInt($(this).val(),10);
		
		
		totalScore = totalScore + elmVal;
		//console.log('elm: ',elmVal );
		
	});

// 1-11 Low
// 12-22 Medium
// 23-34 High
  
     
  var scoreRating = '';
  
  if (totalScore < 12 ) {
      scoreRating = "Low"; 
    } else if ( (totalScore < 23) && (totalScore > 11) ) {
      scoreRating = "Medium";       
    } else if ( totalScore  > 22 ) {
      scoreRating = "High";             
    }
  
	var scoreString = 'Complexity Score: ' + totalScore;
  var scoreRatingString = 'Complexity Rating: ' + scoreRating;
	
	$('#q19952_q16').val(scoreString);
  $('#q19952_q17').val(scoreRatingString);
	//console.log('totalScore: ',totalScore);
	
}





$('#q19951_q1_13').click(function(){
	$('#q19951_q15').focus();	
});



//var alphaSpecialMessage = 'alpha or special characters only';
var alphaSpecialMessage = 'Letters and symbols only';
var digitsMessage = 'Numbers only'
var phoneAuMessage = 'Enter a valid phone number';


//form ID form_email_19948

$.validator.messages.required = "please select an option";

// $.validator.addClassRules("phone-group", {
//     require_from_group: [1, ".phone-group"]
// });



// website enquiry form validation
$('#form_email_19948').validate({
		onfocusout: function(elm) {
			$(elm).valid();
			},
			/*onkeyup: function(element) { //turn off onkeyup validation for ajax checking fields
					//var element_id = jQuery(element).attr('id');
					var element_name = jQuery(element).attr('name');
					if (this.settings.rules[element_name].onkeyup !== false) {
						jQuery.validator.defaults.onkeyup.apply(this, arguments);
					}
				},*/

		rules: {
			'q19950:q1': {
				required: true,
				alphaSpecial:'q19950:q1'
			},
			'q19950:q2': {
				required: true,
				matrixEmail:'q19950:q2',
				//onkeyup:false
			},
			'q19950:q3': {
				required: function(element){
					return ( $('#q19950_q4').is(':blank') );
				},
				phoneAU: true
			},
			'q19950:q4': {
				required: function(element){
					return ( $('#q19950_q3').is(':blank') );
					
				},
				phoneAU: true
			},
			'q19951:q1[]': 'required',
			'q19952:q1': 'required',
			'q19952:q2': 'required',
			'q19952:q3': 'required',
			'q19952:q4': 'required',
			'q19952:q5': 'required',
			'q19952:q6': 'required',
			'q19952:q7': 'required',
			'q19952:q8': 'required',
			'q19952:q9': 'required',
			'q19952:q10': 'required',
			'q19952:q11': 'required',
			'q19952:q12': 'required',
			'q19952:q13': 'required',
			'q19952:q14': 'required',
			'q19952:q15': 'required'
			
			
		},

		messages: {
			'q19950:q1':{
				required:'Enter your name',
				alphaSpecial: alphaSpecialMessage,
			},
			
			'q19950:q2': {
				required: 'Enter a valid email address',
				matrixEmail: 'Enter a valid email address'
			},
			
			'q19950:q3':{
				required: 'Enter your phone or mobile number',
				phoneAU: phoneAuMessage	
			},
			
			'q19950:q4':{
				required: 'Enter your phone or mobile number',
				phoneAU: phoneAuMessage						
			},
			
			'q19951:q1[]':{
				required: 'please select an option'
			},
			
			
				
		},
		
		errorPlacement: function(error, element) {
						if (element.is(":checkbox")) {

							error.insertBefore(element.parent());
							
						}
						
						else if (element.is(":radio")) {

							error.insertBefore(element);
							
						}
						
						else {
							
							error.insertAfter(element);
							
							
						}
						
						//showHideHack();
						
					},






		submitHandler: function(form){
			//here we strip out the design query from the submission url
			var formSubmitUrl = $('#form_email_19948').prop('action');
			formSubmitUrl = formSubmitUrl.split("?")[0].split("#")[0];
			
			$('#form_email_19948').prop('action',formSubmitUrl);
			
			
			calcScore(); 
			form.submit();
			return false;	
		}


	});
	
	
	
	
	// Contact form validation
	$('#form_email_24298').validate({
			onfocusout: function(elm) {
				$(elm).valid();
				},


			rules: {
				'q24302:q1': {
					required: true,
					alphaSpecial:'q24302:q1'
				},
				'q24302:q2': {
					required: true,
					matrixEmail:'q24302:q2'

				},
				'q24302:q3': {
					required: true
				},
				
			},

			messages: {
				'q24302:q1':{
					required:'Enter your name',
					alphaSpecial: alphaSpecialMessage,
				},
			
				'q24302:q2': {
					required: 'Enter a valid email address',
					matrixEmail: 'Enter a valid email address'
				},
				'q24302:q3': {
					required: 'Enter a message'
				},
			
				
			},
		
			
			submitHandler: function(form){
				//here we strip out the design query from the submission url
				var formSubmitUrl = $('#form_email_24298').prop('action');
				formSubmitUrl = formSubmitUrl.split("?")[0].split("#")[0];
			
				$('#form_email_24298').prop('action',formSubmitUrl);
			
			
		
				form.submit();
				return false;	
			}


		});
		
		
var resetNavLinks = function(){
		
		$('ul.sf-menu li a').each(function(e){ 
			
			var homeUrl = 'http://uniweb.monash';
			var theHref = $(this).attr('href');
			var newUrl = homeUrl + theHref;
			
			$(this).attr('href',newUrl);
		
		});
}		
		
		
		
		
		
	